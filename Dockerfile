# build stage
FROM node:lastest
WORKDIR /app
COPY ./package*.json ./
RUN npm install
COPY . .
RUN npm build

# production stage
EXPOSE 3000
CMD ["node", "src/index.js"]